﻿using Asteroids.Core.Base;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Used by <see cref="ObjectPool"/>.
    /// </summary>
    public class PoolElement : BaseBehaviour {

        /// <summary>
        /// If active, this object cannot be reused now. Set to false instead of destroying to be reused by pool. 
        /// </summary>
        public bool IsActive {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

    }

}