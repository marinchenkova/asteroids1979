﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Describes game object motion.
    /// </summary>
    public abstract class Motion : BaseBehaviour {
        
        /// <summary>
        /// Set velocity to change motion.
        /// </summary>
        public abstract Vector2 Velocity { get; set; }
        
    }

}