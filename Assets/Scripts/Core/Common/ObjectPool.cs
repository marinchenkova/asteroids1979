﻿using System.Collections.Generic;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Used for object reusing. Set initial amount of instantiated samples and get them by <see cref="GetElement"/>
    /// method. If there are no pooled disabled objects, this method creates new ones and adds them to pool.
    /// </summary>
    public class ObjectPool : BaseBehaviour {

        [Tooltip("Object that will be instantiated by GetElement method.")]
        [SerializeField]
        private PoolElement _sample;

        [Tooltip("Initial size of pool.")]
        [SerializeField] 
        private int _initialAmount;
        
        private readonly List<PoolElement> _pool = new List<PoolElement>();

        private void Awake() {
            Init();
        }

        private void Init() {
            for (var i = 0; i < _initialAmount; i++) {
                _pool.Add(Create());
            }
        }

        private PoolElement Create() {
            var temp = Instantiate(_sample, transform, true);
            temp.IsActive = false;
            return temp;
        }

        /// <summary>
        /// Returns new instance of sample. If there are no pooled disabled objects,
        /// creates new ones and adds them to pool.
        /// </summary>
        public PoolElement GetElement() {
            foreach (var next in _pool) {
                if (!next.IsActive) {
                    next.IsActive = true;
                    return next;
                }
            }

            var another = Create();
            another.IsActive = true;
            _pool.Add(another);
            
            return another;
        }

    }

}