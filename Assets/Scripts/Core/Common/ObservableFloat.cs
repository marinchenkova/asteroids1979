﻿using Asteroids.Core.Base;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Describes dynamic float value with notification about its changes.
    /// </summary>
    public class ObservableFloat : BaseBehaviour {

        public delegate void ValueChanged(float prev, float curr);
        public event ValueChanged OnValueChanged = delegate {  };
        
        private float _value;
        public float Value {
            get => _value;
            set {
                var prev = _value;
                _value = value;
                OnValueChanged.Invoke(prev, value);
            }
        }
        
    }

}