﻿using System;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Describes one action timer.
    /// </summary>
    public class Timer : BaseBehaviour {

        /// <summary>
        /// Subscribe to this event to receive notification that timer is done.
        /// </summary>
        public event Action OnAction = delegate {  };
        
        private float _lifetime = 1f;
        private float _age = 0f;
        private bool _isActivated = false;

        private void Awake() {
            Stop();
        }

        private void Update() {
            _age += Time.deltaTime;
            if (_age > _lifetime && !_isActivated) {
                _isActivated = true;
                OnAction.Invoke();
            }
        }

        /// <summary>
        /// Set up timer to duration in seconds.
        /// </summary>
        public void Launch(float duration) {
            Stop();
            _lifetime = duration;
            enabled = true;
        }

        /// <summary>
        /// Stop timer.
        /// </summary>
        public void Stop() {
            enabled = false;
            _isActivated = false;
            _age = 0f;
        }
        
    }

}