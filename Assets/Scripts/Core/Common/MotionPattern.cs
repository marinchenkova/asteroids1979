﻿using System;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Describes a bunch of Vector2 passed to subscribers to represent some motion pattern.
    /// </summary>
    public abstract class MotionPattern : BaseBehaviour {
        
        /// <summary>
        /// Subscribe to receive Vector2 objects.
        /// </summary>
        public event Action<Vector2> OnNextElement = delegate {  };

        /// <summary>
        /// Pass Vector2 to subscribers.
        /// </summary>
        /// <param name="dir"></param>
        protected void OnNext(Vector2 dir) {
            OnNextElement.Invoke(dir);
        } 
        
    }

}