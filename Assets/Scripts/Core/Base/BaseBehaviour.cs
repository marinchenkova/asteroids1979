﻿using UnityEngine;

namespace Asteroids.Core.Base {

    /// <summary>
    /// Derive from this class instead of MonoBehaviour to easily apply some changes for all your behaviours.
    /// </summary>
    public abstract class BaseBehaviour : MonoBehaviour {
        
    }

}