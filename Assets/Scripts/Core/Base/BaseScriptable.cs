﻿using UnityEngine;

namespace Asteroids.Core.Base {

    /// <summary>
    /// Derive from this class instead of ScriptableObject to easily apply some changes for all your scriptable objects.
    /// </summary>
    public abstract class BaseScriptable : ScriptableObject {
        
    }

}