﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Audio {

    /// <summary>
    /// AudioPlayer for VolumedClips that are played as one shot.
    /// </summary>
    public class VolumedAudio : BaseBehaviour {

        [Tooltip("Set AudioSource that will be used as one shot source.")]
        [SerializeField]
        private AudioSource _source;

        /// <summary>
        /// Play one shot.
        /// </summary>
        public void Play(VolumedClip clip) {
            clip.PlayOneShotBy(_source);
        }
        
    }

}