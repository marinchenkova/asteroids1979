﻿using System;
using UnityEngine;

namespace Asteroids.Core.Audio {

    /// <summary>
    /// AudioClip with set up volume.
    /// </summary>
    [Serializable]
    public class VolumedClip {
        
        [Range(0f, 1f)]
        [SerializeField]
        private float _volume = 1f;
        public float Volume => _volume;

        [SerializeField]
        private AudioClip _clip;
        public AudioClip Clip => _clip;
        
        public void PlayOneShotBy(AudioSource source) {
            source.PlayOneShot(Clip, Volume);
        }
        
    }

}