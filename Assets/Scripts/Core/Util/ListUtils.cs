﻿using System.Collections.Generic;
using UnityEngine;

namespace Asteroids.Core.Util {

    public static class ListUtils {

        /// <summary>
        /// Returns random list element.
        /// </summary>
        public static T GetRandom<T>(this List<T> list) {
            var index = Random.Range(0, list.Count);
            return list[index];
        }

    }

}