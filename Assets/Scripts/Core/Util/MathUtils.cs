﻿using Asteroids.Data.Enemy;
using Asteroids.Data.Game;
using Asteroids.Data.Saucer;
using UnityEngine;

namespace Asteroids.Core.Util {

    public static class MathUtils {

        /// <summary>
        /// Returns random position on the screen defined in <see cref="ScreenConfig"/>.
        /// </summary>
        public static Vector2 RandomPositionOnScreen(this ScreenConfig config) {
            var x = Random.Range(config.Min.x, config.Max.x);
            var y = Random.Range(config.Min.y, config.Max.y);
            return new Vector2(x, y);
        }
        
        /// <summary>
        /// Returns random position off the screen defined in <see cref="ScreenConfig"/>.
        /// </summary>
        public static Vector2 RandomPositionOffScreen(this ScreenConfig config) {
            var onSide = Random.value < 0.5f;
            var dir = Random.value < 0.5f;
            
            float x;
            float y;
           
            if (onSide) {
                x = dir ? config.Min.x - config.Offset / 2f : config.Max.x + config.Offset / 2f;
                y = Random.Range(config.Min.y, config.Max.y);
            }
            else {
                x = Random.Range(config.Min.x, config.Max.x);
                y = dir ? config.Min.y - config.Offset / 2f : config.Max.y + config.Offset / 2f;
            }
            
            return new Vector2(x, y);
        }

        /// <summary>
        /// Returns random velocity defined by speed in <see cref="EnemyData"/>.
        /// </summary>
        public static Vector2 RandomVelocity(this EnemyData data) {
            var speed = data.RandomSpeed();
            return Random.insideUnitCircle.normalized * speed;
        }
        
        /// <summary>
        /// Returns random speed defined in <see cref="EnemyData"/>.
        /// </summary>
        public static float RandomSpeed(this EnemyData data) {
            return Random.Range(data.MinSpeed, data.MaxSpeed);
        }
        
        /// <summary>
        /// Returns random fire rate defined in <see cref="SaucerData"/>.
        /// </summary>
        public static float RandomFireRate(this SaucerData data) {
            return Random.Range(data.MinFireRate, data.MaxFireRate);
        }

        /// <summary>
        /// Returns random z rotation.
        /// </summary>
        public static Quaternion RandomZRotation() {
            var rotation = Random.rotationUniform;
            var euler = rotation.eulerAngles.z;
            return Quaternion.Euler(0f, 0f, euler);
        }

        /// <summary>
        /// Returns random normalized direction.
        /// </summary>
        public static Vector2 RandomDirection() {
            return Random.insideUnitCircle.normalized;
        }
        
    }

}