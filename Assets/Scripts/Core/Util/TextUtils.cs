﻿namespace Asteroids.Core.Util {

    public static class TextUtils {

        /// <summary>
        /// Score format for Asteroids Game.
        /// </summary>
        public static string FormatScore(int score) {
            var prefix = score < 10 ? "0" : "";
            return $"{prefix}{score}";
        }

    }

}