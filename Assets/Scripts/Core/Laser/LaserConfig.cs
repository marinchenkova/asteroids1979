﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Common {
    
    /// <summary>
    /// Data used by <see cref="LaserGun"/> to do shots.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/LaserConfig", fileName = "LaserConfig")]
    public class LaserConfig : BaseScriptable {

        [Tooltip("Speed of shot in units per second.")]
        [Min(0f)]
        [SerializeField]
        private float _speed = 1f;
        
        /// <summary>
        /// Speed of shot in units per second.
        /// </summary>
        public float Speed => _speed;

        [Tooltip("Lifetime of shot in seconds.")]
        [Min(0f)]
        [SerializeField]
        private float _lifetime = 1f;
        
        /// <summary>
        /// Lifetime of shot in seconds.
        /// </summary>
        public float Lifetime => _lifetime;

        [Tooltip("Damage of shot.")]
        [Min(0f)]
        [SerializeField]
        private float _damage = 1f;
        
        /// <summary>
        /// Damage of shot.
        /// </summary>
        public float Damage => _damage;

        [Tooltip("LaserGun accuracy. 0 represents shooting from -90 to 90 degrees aim offset.")]
        [Min(0f)]
        [SerializeField]
        private float _accuracy = 1f;
        
        /// <summary>
        /// <see cref="LaserGun"/> accuracy. 0 represents shooting from -90 to 90 degrees aim offset.
        /// </summary>
        public float Accuracy => _accuracy;
    }
    
}
