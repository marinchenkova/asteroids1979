﻿using Asteroids.Core.Base;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Represents laser gun that shoots with <see cref="LaserShot"/>.
    /// </summary>
    public abstract class LaserGun : BaseBehaviour {

        /// <summary>
        /// Perform shot.
        /// </summary>
        public abstract void Shoot(LaserShot shot, LaserConfig config);

    }

}