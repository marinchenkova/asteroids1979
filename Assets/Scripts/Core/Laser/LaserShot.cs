﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Core.Common {

    /// <summary>
    /// Used by <see cref="LaserGun"/> to perform shots.
    /// </summary>
    public abstract class LaserShot : BaseBehaviour {

        /// <summary>
        /// Perform shot with parameters.
        /// </summary>
        public abstract void Perform(
            Vector3 position, 
            Quaternion rotation, 
            Vector2 velocity,
            float lifetime,
            float damage
        );

    }

}