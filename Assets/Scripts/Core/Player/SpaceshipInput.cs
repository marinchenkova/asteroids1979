﻿using System;
using Asteroids.Core.Base;

namespace Asteroids.Core.Player {

    /// <summary>
    /// Represents Spaceship inputs. Subscribe to events to receive input control.
    /// </summary>
    public abstract class SpaceshipInput : BaseBehaviour {

        public event Action OnThrust = delegate {  };
        public event Action OnStop = delegate {  };
        public event Action<float> OnYaw = delegate {  };
        public event Action OnShoot = delegate {  };
        public event Action OnHyperspace = delegate {  };

        protected void Thrust() {
            OnThrust.Invoke();
        }
        
        protected void Stop() {
            OnStop.Invoke();
        }
        
        protected void Yaw(float dir) {
            OnYaw.Invoke(dir);
        }
        
        protected void Shoot() {
            OnShoot.Invoke();
        }
        
        protected void Hyperspace() {
            OnHyperspace.Invoke();
        }

    }

}