﻿using Asteroids.Core.Base;

namespace Asteroids.Core.Player {

    /// <summary>
    /// Represents Spaceship audio callbacks.
    /// </summary>
    public abstract class SpaceshipAudio : BaseBehaviour {

        public abstract void PlayThrust();

        public abstract void StopThrust();

        public abstract void PlayShoot();

        public abstract void PlayDestroy();

    }

}