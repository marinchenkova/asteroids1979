﻿using Asteroids.Core.Base;

namespace Asteroids.Core.Player {

    /// <summary>
    /// Represents Spaceship motion by thrust and yaw controls.
    /// </summary>
    public abstract class SpaceshipMotion : BaseBehaviour {

        /// <summary>
        /// Turn thrust on.
        /// </summary>
        public abstract void ThrustOn();

        /// <summary>
        /// Turn thrust off.
        /// </summary>
        public abstract void ThrustOff();
        
        /// <summary>
        /// Make full stop.
        /// </summary>
        public abstract void Stop();

        /// <summary>
        /// Rotate into passed direction.
        /// </summary>
        public abstract void Yaw(float dir);

    }

}