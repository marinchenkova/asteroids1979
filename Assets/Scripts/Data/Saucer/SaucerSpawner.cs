﻿using System;
using System.Collections.Generic;
using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using Asteroids.Core.Util;
using Asteroids.Data.Enemy;
using Asteroids.Data.Game;
using UnityEngine;

namespace Asteroids.Data.Saucer {

    /// <summary>
    /// Spawns saucers with pool.
    /// </summary>
    public class SaucerSpawner : BaseBehaviour {

        /// <summary>
        /// How many saucers are left?
        /// </summary>
        public event Action<int> OnLeftSaucersCountChanged = delegate {  }; 
        
        [SerializeField]
        private ObjectPool _saucerPool;

        [SerializeField]
        private ScreenConfig _screenConfig;

        [SerializeField]
        private EnemyDestructionEffectPlayer _effectPlayer;

        [SerializeField] 
        private VolumedAudio _shootAudio;
        
        [SerializeField]
        private ObjectPool _laserShotPool;

        [SerializeField]
        private ScoreCounter _scoreCounter;

        [SerializeField]
        private Transform _aimTarget;

        [SerializeField]
        private List<SaucerData> _saucers;
        
        private int _spawnCounter = 0;
        
        public void SpawnOffScreen() {
            SpawnOnPosition(_saucers.GetRandom(), _screenConfig.RandomPositionOffScreen());
        }

        private void SpawnOnPosition(SaucerData data, Vector3 position) {
            Spawn(_saucerPool.GetElement().gameObject, data, position);
        }

        public void AfterDestruction(SaucerData data, Vector3 position) {
            _spawnCounter--;
            OnLeftSaucersCountChanged.Invoke(_spawnCounter);
            
            _effectPlayer.PlayDestructionEffect(data, position);
            _scoreCounter.OnEnemyEliminated(data);
        }

        private void Spawn(GameObject enemy, SaucerData data, Vector3 position) {
            enemy.GetComponent<SaucerController>().Init(
                data, 
                this, 
                position, 
                _aimTarget,
                _laserShotPool,
                _shootAudio
            );
            _spawnCounter++;
        }

    }

}