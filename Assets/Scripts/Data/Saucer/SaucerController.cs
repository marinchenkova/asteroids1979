﻿using System;
using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using Asteroids.Core.Util;
using Asteroids.Data.Common;
using Asteroids.Data.Enemy;
using UnityEngine;
using Motion = Asteroids.Core.Common.Motion;

namespace Asteroids.Data.Saucer {

    /// <summary>
    /// Controls saucer.
    /// </summary>
    public class SaucerController : BaseBehaviour {

        [SerializeField]
        private PoolElement _poolElement;
        
        [SerializeField]
        private Motion _motion;

        [SerializeField] 
        private MotionPattern _motionPattern;

        [SerializeField] 
        private Health _health;

        [SerializeField]
        private Damage _damage;
        
        [SerializeField]
        private Aim _aim;

        [SerializeField]
        private LaserController _laserController;
        
        private SaucerData _data;
        private SaucerSpawner _spawner;
        private VolumedAudio _sounds;

        public void Init(
            SaucerData data, 
            SaucerSpawner spawner, 
            Vector3 position, 
            Transform aimTarget, 
            ObjectPool laserShotPool,
            VolumedAudio sounds
        ) {
            _data = data;
            _spawner = spawner;
            _sounds = sounds;
            
            var t = transform;
            t.localScale = Vector3.one * data.Scale;
            t.position = position;

            _laserController.Init(laserShotPool, data.LaserConfig, data.RandomFireRate());
            _aim.Target = aimTarget;
            _damage.Value = data.Damage;
            _health.Value = data.Health;
            
            _laserController.OnShoot += OnShoot;
            _motionPattern.OnNextElement += SetVelocity;
            _health.OnValueChanged += OnHealthChanged;
            _damage.OnValueChanged += OnDamageApplied;
        }

        private void OnHealthChanged(float prev, float curr) {
            if (curr > 0) return;
            OnDestroyed();
        }
        
        private void OnDamageApplied(float prev, float curr) {
            if (curr > 0) return;
            OnDestroyed();
        }

        private void OnShoot() {
            _sounds.Play(_data.ShootClip);
        }
        
        private void OnDestroyed() {
            _motionPattern.OnNextElement -= SetVelocity;
            _health.OnValueChanged -= OnHealthChanged;
            _damage.OnValueChanged -= OnDamageApplied;
            _laserController.OnShoot -= OnShoot;
            _laserController.OnDestroyed();
            _poolElement.IsActive = false;

            _spawner.AfterDestruction(_data, transform.position);
        }

        private void SetVelocity(Vector2 velocity) {
            _motion.Velocity = velocity * _data.RandomSpeed();
        }

    }

}