﻿using Asteroids.Core.Audio;
using Asteroids.Core.Common;
using Asteroids.Data.Enemy;
using UnityEngine;

namespace Asteroids.Data.Saucer {
    
    /// <summary>
    /// Saucer enemy. Saucer can shoot into player.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/SaucerData", fileName = "SaucerData")]
    public class SaucerData : EnemyData {

        [SerializeField]
        private float _scale = 1f;
        public float Scale => _scale;

        [SerializeField]
        private float _minFireRate = 1f;
        public float MinFireRate => _minFireRate;

        [SerializeField]
        private float _maxFireRate = 1f;
        public float MaxFireRate => _maxFireRate;

        [SerializeField] 
        private LaserConfig _laserConfig;
        public LaserConfig LaserConfig => _laserConfig;

        [SerializeField]
        private VolumedClip _shootClip;
        public VolumedClip ShootClip => _shootClip;

    }
    
}
