﻿using System;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using UnityEngine;

namespace Asteroids.Data.Saucer {

    /// <summary>
    /// Saucer laser controller. Uses timer to perform shooting at given fire rate. 
    /// </summary>
    public class LaserController : BaseBehaviour {

        public event Action OnShoot = delegate {  };
        
        [SerializeField]
        private LaserGun _laser;

        [SerializeField]
        private Timer _timer;

        private ObjectPool _laserShotPool;
        private float _fireRate;
        private LaserConfig _config;

        public void Init(ObjectPool laserShotPool, LaserConfig config, float fireRate) {
            _laserShotPool = laserShotPool;
            _fireRate = fireRate;
            _config = config;
            
            _timer.OnAction += Shoot;
            _timer.Launch(fireRate);
        }
        
        public void OnDestroyed() {
            _timer.OnAction -= Shoot;
            _timer.Stop();
        }

        private void Shoot() {
            var shot = _laserShotPool.GetElement().GetComponent<LaserShot>();
            if (shot == null) return;

            _laser.Shoot(shot, _config);
            OnShoot.Invoke();
            
            _timer.Launch(_fireRate);
        }
        
    }

}