﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Game {
    
    /// <summary>
    /// Asteroid Game gameplay settings.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/LevelConfig", fileName = "LevelConfig")]
    public class LevelConfig : BaseScriptable {
        
        [Tooltip("Initial asteroids amount for level.")]
        [Min(0)]
        [SerializeField]
        private int _asteroidAmount = 4;
        
        /// <summary>
        /// Initial asteroids amount for level.
        /// </summary>
        public int AsteroidAmount => _asteroidAmount;

        [Tooltip("Saucers spawn delay in seconds.")]
        [Min(0f)]
        [SerializeField]
        private float _saucerSpawnPause = 10f;
        
        /// <summary>
        /// Saucers spawn delay in seconds.
        /// </summary>
        public float SaucerSpawnPause => _saucerSpawnPause;

        [Tooltip("Player spawn after death delay in seconds.")]
        [Min(0f)]
        [SerializeField]
        private float _spaceshipSpawnPause = 1f;
        
        /// <summary>
        /// Player spawn after death  delay in seconds.
        /// </summary>
        public float SpaceshipSpawnPause => _spaceshipSpawnPause;
        
        [Min(0)]
        [SerializeField]
        private int _spaceshipLives = 4;
        public int SpaceshipLives => _spaceshipLives;
        
        [Tooltip("Pause between levels in seconds.")]
        [Min(0f)]
        [SerializeField]
        private float _betweenLevelPause = 1f;
        
        /// <summary>
        /// Pause between levels in seconds.
        /// </summary>
        public float BetweenLevelPause => _betweenLevelPause;
        
        [Tooltip("Coefficient of initial asteroid amount for next level.")]
        [Min(1f)]
        [SerializeField]
        private float _nextLevelAsteroidAmountMultiplier = 1.5f;
        
        /// <summary>
        /// Coefficient of initial asteroid amount for next level.
        /// </summary>
        public float NextLevelAsteroidAmountMultiplier => _nextLevelAsteroidAmountMultiplier;
        
        [Tooltip("Coefficient of saucer spawn pause for next level.")]
        [Range(0f, 1f)]
        [SerializeField]
        private float _nextLevelSaucerSpawnPauseMultiplier = 0.7f;
        
        /// <summary>
        /// Coefficient of saucer spawn pause for next level.
        /// </summary>
        public float NextLevelSaucerSpawnPauseMultiplier => _nextLevelSaucerSpawnPauseMultiplier;
        
    }
    
}
