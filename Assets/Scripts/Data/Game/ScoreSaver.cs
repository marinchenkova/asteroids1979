﻿using UnityEngine;

namespace Asteroids.Data.Game {

    /// <summary>
    /// Best and current score value store.
    /// </summary>
    public static class ScoreSaver {

        private const string BestScoreKey = "bestScoreKey";
        private const string CurrentScoreKey = "currentScoreKey";

        /// <summary>
        /// Save just gained score.
        /// </summary>
        public static void SaveCurrentScore(int score) {
            PlayerPrefs.SetInt(CurrentScoreKey, score);
        }

        /// <summary>
        /// Forget just gained score.
        /// </summary>
        public static void ResetCurrentScore() {
            SaveCurrentScore(0);
        }
        
        /// <summary>
        /// Save just gained score with entered player name.
        /// </summary>
        public static void SaveCurrentScoreTo(string playerName) {
            var score = PlayerPrefs.GetInt(CurrentScoreKey, 0);
            PlayerPrefs.SetInt(playerName, score);
            
            if (score >= GetBestScore()) {
                PlayerPrefs.SetInt(BestScoreKey, score);
            }
        }

        /// <summary>
        /// Max of best and current score.
        /// </summary>
        public static int GetMaxOfBestOrCurrentScore() {
            return Mathf.Max(GetBestScore(), GetCurrentScore());
        }
        
        public static int GetCurrentScore() {
            return PlayerPrefs.GetInt(CurrentScoreKey, 0);
        }
        
        public static int GetBestScore() {
            return PlayerPrefs.GetInt(BestScoreKey, 0);
        }

    }

}