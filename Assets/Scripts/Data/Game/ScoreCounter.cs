﻿using System;
using Asteroids.Core.Base;
using Asteroids.Data.Enemy;

namespace Asteroids.Data.Game {

    /// <summary>
    /// Sums up score from enemy destruction.
    /// </summary>
    public class ScoreCounter : BaseBehaviour {

        public event Action<int> OnScoreChanged = delegate { };

        /// <summary>
        /// Current score.
        /// </summary>
        public int Score { get; private set; } = 0;

        /// <summary>
        /// Update counter from enemy elimination.
        /// </summary>
        public void OnEnemyEliminated(EnemyData data) {
            Score += data.Score;
            OnScoreChanged.Invoke(Score);
        }

    }

}