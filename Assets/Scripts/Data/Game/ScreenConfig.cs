﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Game {

    /// <summary>
    /// Represents screen space for game.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/ScreenConfig", fileName = "ScreenConfig")]
    public class ScreenConfig : BaseScriptable {
        
        [Tooltip("Bottom left coordinate.")]
        [SerializeField]
        private Vector2 _min;
        
        /// <summary>
        /// Bottom left coordinate.
        /// </summary>
        public Vector2 Min => _min;

        [Tooltip("Top right coordinate.")]
        [SerializeField]
        private Vector2 _max;
        
        /// <summary>
        /// Top right coordinate.
        /// </summary>
        public Vector2 Max => _max;
        
        [Tooltip("Additional space beyond min and max coordinates.")]
        [SerializeField] 
        private float _offset;
        
        /// <summary>
        /// Additional space beyond min and max coordinates.
        /// </summary>
        public float Offset => _offset;

    }

}