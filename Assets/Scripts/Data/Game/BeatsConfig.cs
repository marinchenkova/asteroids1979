﻿using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Game {
    
    /// <summary>
    /// Asteroid music data.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/BeatsConfig", fileName = "BeatsConfig")]
    public class BeatsConfig : BaseScriptable {

        [Tooltip("Initial pause between beats in seconds.")]
        [Min(0)]
        [SerializeField]
        private float _pauseMax = 1f;
        
        /// <summary>
        /// Initial pause between beats in seconds.
        /// </summary>
        public float PauseMax => _pauseMax;
        
        [Tooltip("Pause between beats decrement per second.")]
        [Min(0)]
        [SerializeField]
        private float _pauseDecrement = 0.01f;
        
        /// <summary>
        /// Pause between beats decrement per second.
        /// </summary>
        public float PauseDecrement => _pauseDecrement;
        
        [Tooltip("Final pause between beats in seconds.")]
        [Min(0)]
        [SerializeField]
        private float _pauseMin = 0.2f;
        
        /// <summary>
        /// Final pause between beats in seconds.
        /// </summary>
        public float PauseMin => _pauseMin;
        
        [SerializeField]
        private VolumedClip _beatOne;
        public VolumedClip BeatOne => _beatOne;
        
        [SerializeField]
        private VolumedClip _beatTwo;
        public VolumedClip BeatTwo => _beatTwo;
        
    }
    
}
