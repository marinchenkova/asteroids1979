﻿using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using UnityEngine;

namespace Asteroids.Data.Game {

    /// <summary>
    /// Asteroid Game music.
    /// </summary>
    public class Beats : BaseBehaviour {

        [SerializeField]
        private BeatsConfig _beatsConfig;

        [SerializeField]
        private VolumedAudio _volumedAudio;

        [SerializeField]
        private Timer _timer;

        private float _pause;
        private bool _useBeatOne = true;

        /// <summary>
        /// Start playing with previous rate.
        /// </summary>
        public void Launch() {
            enabled = true;
            _timer.Launch(_pause);
        }

        /// <summary>
        /// Stop playing.
        /// </summary>
        public void Stop() {
            enabled = false;
        }

        /// <summary>
        /// Reset rate to initial.
        /// </summary>
        public void ResetPause() {
            _pause = _beatsConfig.PauseMax;
        }
        
        private void Awake() {
            ResetPause();
        }

        private void OnEnable() {
            _timer.OnAction += PlayNextBeat;
        }

        private void OnDisable() {
            _timer.OnAction -= PlayNextBeat;
        }

        private void Update() {
            _pause -= Time.deltaTime * _beatsConfig.PauseDecrement;
            if (_pause < _beatsConfig.PauseMin) _pause = _beatsConfig.PauseMin;
        }

        private void PlayNextBeat() {
            _volumedAudio.Play(NextBeat());
            _timer.Launch(_pause);
        }

        private VolumedClip NextBeat() {
            var beat = _useBeatOne ? _beatsConfig.BeatOne : _beatsConfig.BeatTwo;
            _useBeatOne = !_useBeatOne;
            return beat;
        }
        
    }

}