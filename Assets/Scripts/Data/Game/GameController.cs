﻿using System;
using System.Collections;
using Asteroids.Core.Base;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asteroids.Data.Game {

    /// <summary>
    /// Asteroid Game controller.
    /// </summary>
    public class GameController : BaseBehaviour {

        /// <summary>
        /// Game start loading.
        /// </summary>
        public event Action OnGameLoading = delegate {  }; 
        
        /// <summary>
        /// Game has started.
        /// </summary>
        public event Action OnGameStarted = delegate {  }; 
        
        /// <summary>
        /// Game over when player's lives value is 0.
        /// </summary>
        public event Action OnGameOver = delegate {  }; 
        
        [SerializeField]
        private LevelController _levelController;

        [SerializeField]
        private ScoreCounter _scoreCounter;
        
        [SerializeField]
        private float _beforeStartPause = 1f;
        
        [SerializeField]
        private float _gameOverPause = 1f;
        
        private void Awake() {
            OnGameLoading.Invoke();
            InvokeDelayed(_beforeStartPause, StartGame);
        }

        private void OnEnable() {
            _levelController.OnGameOver += GameOver;
        }
        
        private void OnDisable() {
            _levelController.OnGameOver -= GameOver;
        }

        private void StartGame() {
            ScoreSaver.ResetCurrentScore();
            OnGameStarted.Invoke();
            _levelController.StartGame();
        }

        private void GameOver() {
            OnGameOver.Invoke();
            InvokeDelayed(_gameOverPause, LoadGameOverScene);
        }

        private void LoadGameOverScene() {
            ScoreSaver.SaveCurrentScore(_scoreCounter.Score);
            SceneManager.LoadScene("MenuSaveScore");
        }

        private void InvokeDelayed(float delay, Action block) {
            StartCoroutine(Delay(delay, block));
        }

        private static IEnumerator Delay(float delay, Action block) {
            yield return new WaitForSeconds(delay);
            block.Invoke();
        }
        
    }

}