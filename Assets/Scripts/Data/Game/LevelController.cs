﻿using System;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using Asteroids.Data.Asteroids;
using Asteroids.Data.Player;
using Asteroids.Data.Saucer;
using UnityEngine;

namespace Asteroids.Data.Game {

    /// <summary>
    /// Asteroid Game levels controller.
    /// </summary>
    public class LevelController : BaseBehaviour {

        /// <summary>
        /// Player's lives count changed.
        /// </summary>
        public event Action<int> OnLivesChanged = delegate {  };
        
        /// <summary>
        /// Game is over when player's lives value is 0.
        /// </summary>
        public event Action OnGameOver = delegate {  };
        
        [SerializeField] 
        private LevelConfig _config;

        [SerializeField]
        private SpaceshipController _spaceshipController;
        
        [SerializeField]
        private AsteroidSpawner _asteroidSpawner;
        
        [SerializeField]
        private SaucerSpawner _saucerSpawner;

        [SerializeField]
        private Beats _beats;

        [SerializeField]
        private Timer _saucerTimer;
        
        [SerializeField]
        private Timer _levelPauseTimer;
        
        [SerializeField]
        private Timer _spaceshipTimer;

        private int _spaceshipLives;
        private int _asteroidAmount;
        private float _saucerSpawnPause;

        private int _asteroidsLeft;
        private int _saucersLeft;

        private void Awake() {
            OnLivesChanged.Invoke(_config.SpaceshipLives);
        }

        private void OnEnable() {
            Subscribe();
        }

        private void OnDisable() {
            StopAllTimers();
            Unsubscribe();
        }

        private void Subscribe() {
            _asteroidSpawner.OnLeftAsteroidsCountChanged += OnLeftAsteroids;
            _saucerSpawner.OnLeftSaucersCountChanged += OnLeftSaucers;
            _saucerTimer.OnAction += SpawnSaucer;
            _levelPauseTimer.OnAction += StartLevel;
            _spaceshipTimer.OnAction += SpawnSpaceship;
            _spaceshipController.OnDied += OnDied;
        }

        private void Unsubscribe() {
            _asteroidSpawner.OnLeftAsteroidsCountChanged -= OnLeftAsteroids;
            _saucerSpawner.OnLeftSaucersCountChanged -= OnLeftSaucers;
            _saucerTimer.OnAction -= SpawnSaucer;
            _levelPauseTimer.OnAction -= StartLevel;
            _spaceshipTimer.OnAction -= SpawnSpaceship;
            _spaceshipController.OnDied -= OnDied;
        }

        /// <summary>
        /// Start game from first level.
        /// </summary>
        public void StartGame() {
            _spaceshipLives = _config.SpaceshipLives;
            _asteroidAmount = _config.AsteroidAmount;
            _saucerSpawnPause = _config.SaucerSpawnPause;
            StartLevel();
            SpawnSpaceship();
        }

        private void FinishGame() {
            StopAllTimers();
            Unsubscribe();
            OnGameOver.Invoke();
        }

        private void StartLevel() {
            _asteroidSpawner.SpawnOffScreen(_asteroidAmount);
            _saucerTimer.Launch(_saucerSpawnPause);
            _levelPauseTimer.Stop();
            _beats.Launch();
        }

        private void FinishLevel() {
            _saucerTimer.Stop();
            _levelPauseTimer.Launch(_config.BetweenLevelPause);
            _beats.Stop();
            _beats.ResetPause();
            _asteroidAmount = Mathf.RoundToInt(_asteroidAmount * _config.NextLevelAsteroidAmountMultiplier);
            _saucerSpawnPause *= _config.NextLevelSaucerSpawnPauseMultiplier;
        }

        private void SpawnSpaceship() {
            _beats.Launch();
            _spaceshipController.Respawn();
        }

        private void SpawnSaucer() {
            _saucerSpawner.SpawnOffScreen();
            _saucerTimer.Launch(_saucerSpawnPause);
        }

        private void OnLeftAsteroids(int count) {
            _asteroidsLeft = count;
            CheckCanFinishLevel();
        }
        
        private void OnLeftSaucers(int count) {
            _saucersLeft = count;
            CheckCanFinishLevel();
        }

        private void CheckCanFinishLevel() {
            if (_saucersLeft == 0 && _asteroidsLeft == 0) {
                FinishLevel();
            }
        }

        private void OnDied() {
            _beats.Stop();
            _spaceshipLives--;
            OnLivesChanged.Invoke(_spaceshipLives);
            
            if (_spaceshipLives == 0) {
                FinishGame();
                return;
            }
            
            _spaceshipTimer.Launch(_config.SpaceshipSpawnPause);
        }

        private void StopAllTimers() {
            _saucerTimer.Stop();
            _levelPauseTimer.Stop();
            _spaceshipTimer.Stop();
        }
        
    }

}