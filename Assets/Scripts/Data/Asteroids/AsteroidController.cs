﻿using Asteroids.Core.Base;
using Asteroids.Core.Common;
using Asteroids.Core.Util;
using Asteroids.Data.Enemy;
using UnityEngine;
using Motion = Asteroids.Core.Common.Motion;

namespace Asteroids.Data.Asteroids {

    /// <summary>
    /// Controls pooled asteroid object.
    /// </summary>
    public class AsteroidController : BaseBehaviour {
        
        [SerializeField]
        private PoolElement _poolElement;
        
        [SerializeField] 
        private Health _health;
        
        [SerializeField]
        private Damage _damage;

        [SerializeField] 
        private SpriteRenderer _renderer;

        [SerializeField] 
        private Motion _motion;
        
        private AsteroidData _data;
        private AsteroidSpawner _spawner;
        
        /// <summary>
        /// Invoked from <see cref="AsteroidSpawner"/> to set up initial parameters.
        /// </summary>
        public void Init(AsteroidData data, AsteroidSpawner spawner, Sprite sprite, Vector3 position) {
            _data = data;
            _spawner = spawner;

            var t = transform;
            t.localScale = Vector3.one * data.Scale;
            t.SetPositionAndRotation(position, MathUtils.RandomZRotation());
            
            _renderer.sprite = sprite;
            _motion.Velocity = data.RandomVelocity();
            _damage.Value = data.Damage;
            _health.Value = data.Health;
            _damage.OnValueChanged += OnDamageApplied;
            _health.OnValueChanged += OnHealthChanged;
        }

        private void OnHealthChanged(float prev, float curr) {
            if (curr > 0f) return;
            OnDestroyed();
        }

        private void OnDamageApplied(float prev, float curr) {
            if (curr > 0f) return;
            OnDestroyed();
        }

        private void OnDestroyed() {
            _damage.OnValueChanged -= OnDamageApplied;
            _health.OnValueChanged -= OnHealthChanged;
            _poolElement.IsActive = false;

            _spawner.AfterDestruction(_data, transform.position);
        }
        
    }

}