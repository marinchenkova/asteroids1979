﻿using System.Collections.Generic;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Asteroids {
    
    /// <summary>
    /// Contains sprites for asteroids.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/AsteroidSpawnConfig", fileName = "AsteroidSpawnConfig")]
    public class AsteroidSpawnConfig : BaseScriptable {

        [SerializeField]
        private List<Sprite> _sprites;
        public List<Sprite> Sprites => _sprites;
        
    }

}