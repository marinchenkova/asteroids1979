﻿using Asteroids.Data.Enemy;
using UnityEngine;

namespace Asteroids.Data.Asteroids {
    
    /// <summary>
    /// Represents Asteroid enemy.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/AsteroidData", fileName = "AsteroidData")]
    public class AsteroidData : EnemyData {

        [Tooltip("Scale of asteroid.")]
        [SerializeField]
        private float _scale = 1f;
        
        /// <summary>
        /// Scale of asteroid.
        /// </summary>
        public float Scale => _scale;

        [Tooltip("How many asteroids should be spawned after destruction?")]
        [SerializeField]
        private float _spawnAfterDestructionAmount = 2f;
        
        /// <summary>
        /// How many asteroids should be spawned after destruction?
        /// </summary>
        public float SpawnAfterDestructionAmount => _spawnAfterDestructionAmount;
        
    }
    
}
