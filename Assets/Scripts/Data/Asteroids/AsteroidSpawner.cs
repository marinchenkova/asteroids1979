﻿using System;
using System.Collections.Generic;
using System.Linq;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using Asteroids.Core.Util;
using Asteroids.Data.Enemy;
using Asteroids.Data.Game;
using UnityEngine;

namespace Asteroids.Data.Asteroids {

    /// <summary>
    /// Spawns asteroids with pool.
    /// </summary>
    public class AsteroidSpawner : BaseBehaviour {

        /// <summary>
        /// How many asteroids have left?
        /// </summary>
        public event Action<int> OnLeftAsteroidsCountChanged = delegate {  }; 
        
        [SerializeField]
        private ObjectPool _asteroidPool;
        
        [SerializeField]
        private ScreenConfig _screenConfig;
        
        [SerializeField]
        private AsteroidSpawnConfig _asteroidSpawnConfig;

        [SerializeField]
        private EnemyDestructionEffectPlayer _effectPlayer;

        [SerializeField]
        private ScoreCounter _scoreCounter;
        
        [Tooltip("Set up asteroids data to represent spawn hierarchy. Last element will spawn nothing.")]
        [SerializeField]
        private List<AsteroidData> _typeHierarchy;

        private int _spawnCounter = 0;
        
        /// <summary>
        /// Spawns amount asteroids off screen.
        /// </summary>
        public void SpawnOffScreen(int amount) {
            var data = _typeHierarchy.First();
            for (var i = 0; i < amount; i++) {
                SpawnOnPosition(data, _screenConfig.RandomPositionOffScreen());
            }
        }

        /// <summary>
        /// Spawns asteroids if needed and plays destruction effects.
        /// </summary>
        public void AfterDestruction(AsteroidData data, Vector3 position) {
            _effectPlayer.PlayDestructionEffect(data, position);
            _scoreCounter.OnEnemyEliminated(data);
            
            var spawnData = GetSpawnDataFrom(data);
            if (spawnData == null) {
                DecrementCounterAndNotify();
                return;
            }
            
            for (var i = 0; i < data.SpawnAfterDestructionAmount; i++) {
                SpawnOnPosition(spawnData, position);
            }
            
            DecrementCounterAndNotify();
        }

        private void DecrementCounterAndNotify() {
            _spawnCounter--;
            OnLeftAsteroidsCountChanged.Invoke(_spawnCounter);
        }

        private void SpawnOnPosition(AsteroidData data, Vector3 position) {
            Spawn(_asteroidPool.GetElement().gameObject, data, position);
        }

        private void Spawn(GameObject enemy, AsteroidData data, Vector3 position) {
            enemy.GetComponent<AsteroidController>().Init(
                data,
                this,
                _asteroidSpawnConfig.Sprites.GetRandom(),
                position
            );
            
            _spawnCounter++;
        }

        private AsteroidData GetSpawnDataFrom(AsteroidData data) {
            var index = _typeHierarchy.IndexOf(data);
            return index < _typeHierarchy.Count - 1 ? _typeHierarchy[index + 1] : null;
        }

    }

}