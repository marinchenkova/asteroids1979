﻿using Asteroids.Core.Player;
using UnityEngine;

namespace Asteroids.Data.Player {

    /// <summary>
    /// Spaceship input with old input system. 
    /// </summary>
    public class ClassicSpaceshipInput : SpaceshipInput {

        [SerializeField]
        private SpaceshipInputBindings _bindings;

        private void Update() {
            ReadRotationInput();
            ReadMotionInput();
            ReadLaserInput();
            ReadHyperspaceInput();
        }

        private void ReadRotationInput() {
            var left = Input.GetKey(_bindings.YawLeft) ? 1 : 0;
            var right = Input.GetKey(_bindings.YawRight) ? -1 : 0;
            var dir = left + right;
            Yaw(dir);
        }
        
        private void ReadMotionInput() {
            if (Input.GetKeyDown(_bindings.Thrust)) {
                Thrust();
                return;
            }
            
            if (Input.GetKeyUp(_bindings.Thrust)) {
                Stop();
            }
        }

        private void ReadLaserInput() {
            if (Input.GetKeyDown(_bindings.Shoot)) {
                Shoot();
            }
        }

        private void ReadHyperspaceInput() {
            if (Input.GetKeyDown(_bindings.Hyperspace)) {
                Hyperspace();
            }
        }

    }

}