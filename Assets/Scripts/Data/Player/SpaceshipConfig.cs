﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Player {

    [CreateAssetMenu(menuName = "Asteroids/SpaceshipConfig", fileName = "SpaceshipConfig")]
    public class SpaceshipConfig : BaseScriptable {
        
        [Tooltip("Speed of speed increment when thrust is on.")]
        [Min(0f)]
        [SerializeField]
        private float _acceleration = 1f;
        
        /// <summary>
        /// Speed of speed increment when thrust is on.
        /// </summary>
        public float Acceleration => _acceleration;
        
        [Tooltip("Speed of speed decrement when thrust is off.")]
        [Min(0f)]
        [SerializeField]
        private float _drag = 1f;
        
        /// <summary>
        /// Speed of speed decrement when thrust is off.
        /// </summary>
        public float Drag => _drag;

        [Min(0f)]
        [SerializeField]
        private float _maxSpeed = 1f;
        public float MaxSpeed => _maxSpeed;

        [Min(0f)]
        [SerializeField]
        private float _yawSpeed = 1f;
        public float YawSpeed => _yawSpeed;

        [Min(0f)]
        [SerializeField]
        private float _health = 1f;
        public float Health => _health;

        [Tooltip("Respawn delay in seconds after use hyperspace ability.")]
        [Min(0f)]
        [SerializeField]
        private float _hyperspacePause = 1f;
        
        /// <summary>
        /// Respawn delay in seconds after use hyperspace ability.
        /// </summary>
        public float HyperspacePause => _hyperspacePause;
        
    }

}