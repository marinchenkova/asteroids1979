﻿using System;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using Asteroids.Core.Player;
using Asteroids.Core.Util;
using Asteroids.Data.Enemy;
using Asteroids.Data.Game;
using UnityEngine;

namespace Asteroids.Data.Player {

    /// <summary>
    /// Remote Spaceship controller.
    /// </summary>
    public class SpaceshipController : BaseBehaviour {

        public event Action OnDied = delegate {  };
        
        [SerializeField]
        private GameObject _spaceship;

        [SerializeField] 
        private GameObject _thrust;

        [SerializeField]
        private SpaceshipConfig _config;
        
        [SerializeField]
        private ScreenConfig _screenConfig;

        [SerializeField]
        private SpaceshipInput _input;

        [SerializeField]
        private SpaceshipMotion _motion;
        
        [SerializeField]
        private ObjectPool _laserShotsPool;
        
        [SerializeField]
        private SpaceshipAudio _spaceshipAudio;
        
        [SerializeField]
        private ParticleSystem _destroyEffect;

        [SerializeField]
        private LaserConfig _laserConfig;
        
        [SerializeField]
        private LaserGun _laserGun;
        
        [SerializeField]
        private Health _health;

        [SerializeField]
        private Timer _timer;
        
        private bool _isSubscribed = false;

        /// <summary>
        /// Respawn in center.
        /// </summary>
        public void Respawn() {
            Respawn(Vector3.zero, Quaternion.identity);
        }
        
        private void Respawn(Vector3 position, Quaternion rotation) {
            if (!_isSubscribed) Subscribe();
            Stop();
            _health.Value = _config.Health;
            _spaceship.transform.SetPositionAndRotation(position, rotation);
            _spaceship.SetActive(true);
        }
        
        private void RespawnInRandom() {
            _timer.Stop();
            _timer.OnAction -= RespawnInRandom;
            var position = _screenConfig.RandomPositionOnScreen();
            Respawn(position, Quaternion.identity);
        }

        private void Die() {
            Disappear();
            _spaceshipAudio.PlayDestroy();
            _destroyEffect.transform.position = _spaceship.transform.position;
            _destroyEffect.gameObject.SetActive(true);
            _destroyEffect.Play();
            OnDied.Invoke();
        }

        private void Disappear() {
            Unsubscribe();
            Stop();
            _spaceship.gameObject.SetActive(false);
        }

        private void Stop() {
            _motion.Stop();
            _spaceshipAudio.StopThrust();
            _thrust.SetActive(false);
        }

        private void Subscribe() {
            _isSubscribed = true;
            _input.OnThrust += OnThrust;
            _input.OnStop += OnStop;
            _input.OnYaw += OnYaw;
            _input.OnShoot += OnShoot;
            _input.OnHyperspace += OnHyperspace;
            _health.OnValueChanged += OnHealthChanged;
        }

        private void Unsubscribe() {
            _isSubscribed = false;
            _input.OnThrust -= OnThrust;
            _input.OnStop -= OnStop;
            _input.OnYaw -= OnYaw;
            _input.OnShoot -= OnShoot;
            _input.OnHyperspace -= OnHyperspace;
            _health.OnValueChanged -= OnHealthChanged;
        }

        private void OnHealthChanged(float prev, float curr) {
            if (curr > 0) return;
            Disappear();
            Die();
        }

        private void OnThrust() {
            _thrust.SetActive(true);
            _motion.ThrustOn();
            _spaceshipAudio.PlayThrust();
        }

        private void OnStop() {
            _thrust.SetActive(false);
            _motion.ThrustOff();
            _spaceshipAudio.StopThrust();
        }

        private void OnYaw(float dir) {
            _motion.Yaw(dir);
        }

        private void OnShoot() {
            var shot = _laserShotsPool.GetElement().GetComponent<LaserShot>();
            if (shot == null) return;
            
            _laserGun.Shoot(shot, _laserConfig);
            _spaceshipAudio.PlayShoot();
        }
        
        private void OnHyperspace() {
            Disappear();
            _timer.OnAction += RespawnInRandom;
            _timer.Launch(_config.HyperspacePause);
        }

    }

}