﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Player {

    [CreateAssetMenu(menuName = "Asteroids/SpaceshipInputBindings", fileName = "SpaceshipInputBindings")]
    public class SpaceshipInputBindings : BaseScriptable {
        
        [SerializeField]
        private KeyCode _thrust = KeyCode.W;
        public KeyCode Thrust => _thrust;
        
        [SerializeField]
        private KeyCode _yawLeft = KeyCode.A;
        public KeyCode YawLeft => _yawLeft;
        
        [SerializeField]
        private KeyCode _yawRight = KeyCode.D;
        public KeyCode YawRight => _yawRight;
        
        [SerializeField]
        private KeyCode _shoot = KeyCode.RightControl;
        public KeyCode Shoot => _shoot;

        [SerializeField]
        private KeyCode _hyperspace = KeyCode.H;
        public KeyCode Hyperspace => _hyperspace;
        
    }

}