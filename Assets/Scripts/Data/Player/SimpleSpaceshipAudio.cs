﻿using Asteroids.Core.Audio;
using Asteroids.Core.Player;
using UnityEngine;

namespace Asteroids.Data.Player {

    /// <summary>
    /// Spaceship audio which uses separate sources for effects and thrust.
    /// </summary>
    public class SimpleSpaceshipAudio : SpaceshipAudio {

        [SerializeField]
        private AudioSource _thrustSource;
        
        [SerializeField]
        private VolumedAudio _effectAudio;

        [SerializeField] 
        private SpaceshipAudioConfig _config;

        private void Awake() {
            _thrustSource.clip = _config.ThrustClip.Clip;
            _thrustSource.playOnAwake = false;
            
        }

        private void OnEnable() {
            _thrustSource.loop = true;
        }

        private void OnDisable() {
            _thrustSource.loop = false;
            _thrustSource.Stop();
        }

        public override void PlayThrust() {
            _thrustSource.Play();
        }

        public override void StopThrust() {
            _thrustSource.Stop();
        }

        public override void PlayShoot() {
            _effectAudio.Play(_config.ShootClip);
        }

        public override void PlayDestroy() {
            _effectAudio.Play(_config.DestroyClip);
        }

    }

}