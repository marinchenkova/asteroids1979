﻿using Asteroids.Core.Player;
using UnityEngine;

namespace Asteroids.Data.Player {

    /// <summary>
    /// Spaceship motion with drag. 
    /// </summary>
    public class DraggingSpaceshipMotion : SpaceshipMotion {

        [SerializeField]
        private SpaceshipConfig _config;
        
        private Transform _transform;
        private Vector3 _targetVelocity = Vector3.zero;
        private Vector3 _velocity = Vector3.zero;
        private float _speed = 0f;
        private bool _isThrusting = false;

        public override void ThrustOn() {
            _isThrusting = true;
        }

        public override void ThrustOff() {
            _isThrusting = false;
        }

        public override void Stop() {
            _isThrusting = false;
            _speed = 0f;
            _targetVelocity = Vector3.zero;
            _velocity = Vector3.zero;
        }

        public override void Yaw(float dir) {
            _transform.Rotate(0f, 0f, _config.YawSpeed * dir);
        }

        private void Awake() {
            _transform = transform;
        }

        private void Update() {
            _speed = CalculateThrust();
            _targetVelocity = CalculateTargetVelocity();
            _velocity = CalculateVelocity();
            Move(_velocity);
        }

        private float CalculateThrust() {
            if (_isThrusting) {
                var target = _speed + _config.Acceleration * Time.deltaTime;
                var clamped = Mathf.Clamp(target, 0f, _config.MaxSpeed);
                return clamped;
            }
                
            return 0f;
        }

        private Vector3 CalculateTargetVelocity() {
            return _speed * transform.up;
        }

        private Vector3 CalculateVelocity() {
            var acceleration = Time.deltaTime * _config.Drag;
            return Vector3.Lerp(_velocity, _targetVelocity, acceleration);
        }

        private void Move(Vector3 delta) {
            _transform.Translate(delta, Space.World);
        }
        
    }

}