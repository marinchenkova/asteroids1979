﻿using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Player {

    /// <summary>
    /// Spaceship audio clips.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/SpaceshipAudioConfig", fileName = "SpaceshipAudioConfig")]
    public class SpaceshipAudioConfig : BaseScriptable {
        
        [SerializeField] 
        private VolumedClip _thrustClip;
        public VolumedClip ThrustClip => _thrustClip;
        
        [SerializeField] 
        private VolumedClip _shootClip;
        public VolumedClip ShootClip => _shootClip;

        [SerializeField] 
        private VolumedClip _destroyClip;
        public VolumedClip DestroyClip => _destroyClip;
        
    }

}