﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Common {

    /// <summary>
    /// Looks at target.
    /// </summary>
    public class Aim : BaseBehaviour {

        /// <summary>
        /// Aim target.
        /// </summary>
        public Transform Target { get; set; }

        private Transform _subject;

        private void Awake() {
            _subject = transform;
        }

        private void Update() {
            var targetPosition = Target.position;
            var subjectPosition = _subject.position;
            var targetDir = targetPosition - subjectPosition;
            var currentDir = _subject.up;
            var rotation = Quaternion.FromToRotation(currentDir, targetDir);
            _subject.rotation *= rotation;
        }
        
    }

}