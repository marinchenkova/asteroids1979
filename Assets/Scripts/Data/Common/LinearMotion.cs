﻿using UnityEngine;
using Motion = Asteroids.Core.Common.Motion;

namespace Asteroids.Data.Common {

    /// <summary>
    /// Objects moves continuously with set up velocity.
    /// </summary>
    public class LinearMotion : Motion {
        
        public override Vector2 Velocity { get; set; }

        private Transform _transform;
        
        private void Awake() {
            _transform = transform;
        }

        private void Update() {
            var motion = Time.deltaTime * Velocity;
            _transform.Translate(motion, Space.World);
        }
        
    }

}