﻿using Asteroids.Core.Base;
using Asteroids.Core.Util;
using Asteroids.Data.Game;
using UnityEngine;

namespace Asteroids.Data.Common {

    /// <summary>
    /// Represents screen bounds wrapper, which prevents object from being out of game space
    /// defined in <see cref="ScreenConfig"/> .
    /// </summary>
    public class BoundMirror : BaseBehaviour {

        [SerializeField]
        private ScreenConfig _config;

        private Transform _transform;
        private bool _xWasOff = false;
        private bool _yWasOff = false;

        private float _timer;
        
        private void Awake() {
            _transform = transform;
        }

        private void Update() {
            CheckScreenPosition();
            CheckOffScreenTooLong();
        }

        private void CheckScreenPosition() {
            if (_xWasOff && _yWasOff) return;
            
            var position = _transform.position;

            var xIsOff = XIsOff(position);
            var yIsOff = YIsOff(position);

            if (!_xWasOff && xIsOff) {
                position.x = Revert(position.x);
                _xWasOff = true;
            }

            if (!_yWasOff && yIsOff) {
                position.y = Revert(position.y);
                _yWasOff = true;
            }

            if (!xIsOff) _xWasOff = false;
            if (!yIsOff) _yWasOff = false;

            _transform.position = position;
        }

        private void CheckOffScreenTooLong() {
            if (_xWasOff || _yWasOff) {
                _timer += Time.deltaTime;
                if (_timer > 2f) {
                    _timer = 0f;
                    _transform.position = _config.RandomPositionOffScreen();
                }
            }
        }

        private float Revert(float t) {
            return -Mathf.Sign(t) * (Mathf.Abs(t) - _config.Offset);
        }

        private bool XIsOff(Vector3 position) {
            return position.x <= _config.Min.x - _config.Offset || _config.Max.x + _config.Offset <= position.x;
        }

        private bool YIsOff(Vector3 position) {
            return position.y <= _config.Min.y - _config.Offset || _config.Max.y + _config.Offset <= position.y;
        }

    }

}