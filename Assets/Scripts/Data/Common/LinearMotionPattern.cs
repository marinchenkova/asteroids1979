﻿using System.Collections.Generic;
using Asteroids.Core.Common;
using Asteroids.Core.Util;
using UnityEngine;

namespace Asteroids.Data.Common {

    /// <summary>
    /// Represents Vector2 objects which are emitted by timer.
    /// </summary>
    public class LinearMotionPattern : MotionPattern {
        
        [Tooltip("Random order passed elements.")]
        [SerializeField]
        private List<Vector2> _motionElements;

        [Tooltip("If true, random Vector2 will be generated.")]
        [SerializeField]
        private bool _useRandom = false;
        
        [Tooltip("Min timer duration for next emission.")]
        [SerializeField]
        [Min(0f)]
        private float _elementDurationMin = 1f;
        
        [Tooltip("Max timer duration for next emission.")]
        [SerializeField]
        [Min(0f)]
        private float _elementDurationMax = 1f;

        [SerializeField]
        private Timer _timer;
        
        private void OnEnable() {
            _timer.OnAction += OnTimerAction;
            _timer.Launch(0f);
        }

        private void OnDisable() {
            _timer.OnAction -= OnTimerAction;
            _timer.Stop();
        }

        private void OnTimerAction() {
            OnNext(GetNextDirection());
            _timer.Launch(GetRandomDuration());
        }
        
        private Vector2 GetNextDirection() {
            if (_useRandom || _motionElements.Count == 0) {
                _useRandom = true;
                return MathUtils.RandomDirection();
            }

            return _motionElements.GetRandom();
        }

        private float GetRandomDuration() {
            return Random.Range(_elementDurationMin, _elementDurationMax);
        }
        
    }

}