﻿using Asteroids.Core.Common;
using Asteroids.Data.Enemy;
using UnityEngine;
using Motion = Asteroids.Core.Common.Motion;

namespace Asteroids.Data.Laser {

    /// <summary>
    /// LaserShot with pooling. It destroys when damage applied.
    /// </summary>
    public class PooledLaserShot : LaserShot {

        [SerializeField]
        private PoolElement _poolElement;
        
        [SerializeField]
        private Damage _damage;
        
        [SerializeField]
        private Timer _timer;

        [SerializeField]
        private Motion _motion;

        public override void Perform(Vector3 position, Quaternion rotation, Vector2 velocity, float lifetime, float damage) {
            _damage.OnValueChanged += OnDamageApplied;
            _timer.OnAction += Recycle;
            
            transform.SetPositionAndRotation(position, rotation);
            _motion.Velocity = velocity;
            _damage.Value = damage;
            _timer.Launch(lifetime);
        }

        private void OnDamageApplied(float prev, float curr) {
            if (curr > 0f) return;
            Recycle();
        }

        private void Recycle() {
            _damage.OnValueChanged -= OnDamageApplied;
            _timer.OnAction -= Recycle;
            _poolElement.IsActive = false;
            _timer.Stop();
        }
        
    }

}