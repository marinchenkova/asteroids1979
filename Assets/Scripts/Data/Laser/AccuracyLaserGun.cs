﻿using Asteroids.Core.Common;
using Asteroids.Core.Util;
using UnityEngine;

namespace Asteroids.Data.Laser {

    /// <summary>
    /// LaserGun with simple accuracy algorithm.
    /// </summary>
    public class AccuracyLaserGun : LaserGun {

        private Transform _gun;

        private void Awake() {
            _gun = transform;
        }

        public override void Shoot(LaserShot shot, LaserConfig config) {
            var position = _gun.position;
            var dir = ConsiderAccuracy(config, _gun.up);
            var rotation = Quaternion.LookRotation(_gun.forward, dir);
            var velocity =  dir * config.Speed;
            var lifetime = config.Lifetime;
            var damage = config.Damage;
            
            shot.Perform(position, rotation, velocity, lifetime, damage);
        }

        private static Vector2 ConsiderAccuracy(LaserConfig config, Vector2 dir) {
            var accuracy = MathUtils.RandomDirection() * (1 - config.Accuracy);
            var edited = dir + accuracy;
            return edited.normalized;
        }

    }

}