﻿using Asteroids.Core.Base;
using Asteroids.Core.Util;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.Data.UI {

    /// <summary>
    /// Updates player's score.
    /// </summary>
    public class PlayerScoreUi : BaseBehaviour {

        [SerializeField]
        private Text _playerScore;

        [SerializeField]
        private ScoreCounter _scoreCounter;
        
        private void OnEnable() {
            _scoreCounter.OnScoreChanged += OnScoreChanged;
        }
        
        private void OnDisable() {
            _scoreCounter.OnScoreChanged -= OnScoreChanged;
        }

        private void OnScoreChanged(int value) {
            var score = TextUtils.FormatScore(value);
            _playerScore.text = score;
        }
        
    }

}