﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.UI {

    [CreateAssetMenu(menuName = "Asteroids/MenuSaveScoreInputBindings", fileName = "MenuSaveScoreInputBindings")]
    public class MenuSaveScoreInputBindings : BaseScriptable {

        [SerializeField]
        private KeyCode _rotateLeft = KeyCode.A;
        public KeyCode RotateLeft => _rotateLeft;
        
        [SerializeField]
        private KeyCode _rotateRight = KeyCode.D;
        public KeyCode RotateRight => _rotateRight;
        
        [SerializeField]
        private KeyCode _ok = KeyCode.H;
        public KeyCode Ok => _ok;

        [SerializeField]
        private KeyCode _restart = KeyCode.R;
        public KeyCode Restart => _restart;
        
    }

}