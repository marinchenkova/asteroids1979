﻿using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using Asteroids.Data.Asteroids;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asteroids.Data.UI {

    public class MenuStartController : BaseBehaviour {

        [SerializeField]
        private MenuStartInput _menuStartInput;

        [SerializeField]
        private AsteroidSpawner _asteroidSpawner;

        [SerializeField]
        private int _asteroidAmount = 4;

        [SerializeField]
        private VolumedAudio _volumedAudio;

        [SerializeField] 
        private VolumedClip _coinClip;
        
        private void Awake() {
            _asteroidSpawner.SpawnOffScreen(_asteroidAmount);
        }

        private void OnEnable() {
            _menuStartInput.OnPressPlay += StartGame;
            _menuStartInput.OnPressCoin += PlayCoin;
        }

        private void OnDisable() {
            _menuStartInput.OnPressPlay -= StartGame;
            _menuStartInput.OnPressCoin -= PlayCoin;
        }
        
        public void StartGame() {
            SceneManager.LoadScene("Gameplay");
        }

        public void PlayCoin() {
            _volumedAudio.Play(_coinClip);
        }

    }

}