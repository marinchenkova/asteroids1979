﻿using System;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.UI {

    public class MenuStartInput : BaseBehaviour {

        public event Action OnPressPlay = delegate {  };
        public event Action OnPressCoin = delegate {  };
        
        [SerializeField]
        private MenuStartInputBindings _bindings;

        private void Update() {
            if (Input.GetKeyDown(_bindings.Play)) {
                OnPressPlay.Invoke();
            }
            
            if (Input.GetKeyDown(_bindings.Coin)) {
                OnPressCoin.Invoke();
            }
        }
        
    }

}