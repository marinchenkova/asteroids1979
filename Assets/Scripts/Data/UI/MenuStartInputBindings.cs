﻿using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.UI {

    [CreateAssetMenu(menuName = "Asteroids/MenuStartInputBindings", fileName = "MenuStartInputBindings")]
    public class MenuStartInputBindings : BaseScriptable {

        [SerializeField]
        private KeyCode _play = KeyCode.P;
        public KeyCode Play => _play;
        
        [SerializeField]
        private KeyCode _coin = KeyCode.C;
        public KeyCode Coin => _coin;

    }

}