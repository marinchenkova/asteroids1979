﻿using Asteroids.Core.Base;
using Asteroids.Core.Util;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.Data.UI {

    /// <summary>
    /// Set current score to label.
    /// </summary>
    public class CurrentScoreUi : BaseBehaviour {

        [SerializeField]
        private Text _currentScoreText;

        private void Awake() {
            var currentScore = TextUtils.FormatScore(ScoreSaver.GetCurrentScore());
            _currentScoreText.text = currentScore;
        }

    }

}