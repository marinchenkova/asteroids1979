﻿using Asteroids.Core.Base;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.Data.UI {

    /// <summary>
    /// Set center label while in gameplay.
    /// </summary>
    public class CenterLabelUi : BaseBehaviour {

        [SerializeField] 
        private GameController _controller;

        [SerializeField]
        private Text _centerLabel;

        [SerializeField]
        private string _onGameLoadingText;
        
        [SerializeField]
        private string _onGameOverText;
        
        private void OnEnable() {
            _controller.OnGameLoading += OnGameLoading;
            _controller.OnGameStarted += OnGameStarted;
            _controller.OnGameOver += OnGameOver;
        }
        
        private void OnDisable() {
            _controller.OnGameLoading -= OnGameLoading;
            _controller.OnGameStarted -= OnGameStarted;
            _controller.OnGameOver -= OnGameOver;
        }

        private void OnGameLoading() {
            _centerLabel.text = _onGameLoadingText;
        }
        
        private void OnGameStarted() {
            _centerLabel.enabled = false;
        }
        
        private void OnGameOver() {
            _centerLabel.enabled = true;
            _centerLabel.text = _onGameOverText;
        }
        
    }

}