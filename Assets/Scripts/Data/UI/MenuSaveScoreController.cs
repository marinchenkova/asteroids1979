﻿using Asteroids.Core.Base;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Asteroids.Data.UI {

    public class MenuSaveScoreController : BaseBehaviour {

        [SerializeField]
        private MenuSaveScoreInput _menuSaveScoreInput;

        [SerializeField]
        private NameInputUi _nameInput;

        private void OnEnable() {
            _menuSaveScoreInput.OnPressOk += OnPressOk;
            _menuSaveScoreInput.OnPressRestart += OnPressRestart;
            _menuSaveScoreInput.OnPressRotateLeft += OnPressRotateLeft;
            _menuSaveScoreInput.OnPressRotateRight += OnPressRotateRight;
        }

        private void OnDisable() {
            _menuSaveScoreInput.OnPressOk -= OnPressOk;
            _menuSaveScoreInput.OnPressRestart -= OnPressRestart;
            _menuSaveScoreInput.OnPressRotateLeft -= OnPressRotateLeft;
            _menuSaveScoreInput.OnPressRotateRight -= OnPressRotateRight;
        }

        private void OnPressOk() {
            _nameInput.OnLetterOk();
        }

        private void OnPressRotateLeft() {
            _nameInput.OnPrevLetter();
        }

        private void OnPressRotateRight() {
            _nameInput.OnNextLetter();
        }

        public void OnPressRestart() {
            ScoreSaver.ResetCurrentScore();
            SceneManager.LoadScene("Gameplay");
        }

    }

}