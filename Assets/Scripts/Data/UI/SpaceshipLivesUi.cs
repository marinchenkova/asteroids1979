﻿using Asteroids.Core.Base;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.Data.UI {

    /// <summary>
    /// Updates player's lives.
    /// </summary>
    public class SpaceshipLivesUi : BaseBehaviour {

        [SerializeField]
        private Transform _livesContainer;

        [SerializeField]
        private GameObject _lifePrefab;
        
        [SerializeField]
        private LevelController _controller;

        private float _spriteWidth;
        
        private void Awake() {
            _spriteWidth = _lifePrefab.GetComponent<RawImage>().rectTransform.sizeDelta.x;
        }

        private void OnEnable() {
            _controller.OnLivesChanged += OnLivesChanged;
        }
        
        private void OnDisable() {
            _controller.OnLivesChanged -= OnLivesChanged;
        }

        private void OnLivesChanged(int count) {
            var childCount = _livesContainer.childCount;
            if (childCount == count) return;
            if (childCount > count) {
                var child = _livesContainer.GetChild(childCount - 1);
                Destroy(child.gameObject);
                return;
            }

            for (var i = childCount; i < count; i++) {
                var life = Instantiate(_lifePrefab, _livesContainer);
                life.transform.Translate(i * _spriteWidth, 0f, 0f);
            }
        }
        
    }

}