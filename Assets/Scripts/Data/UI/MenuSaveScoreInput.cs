﻿using System;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.UI {

    public class MenuSaveScoreInput : BaseBehaviour {

        public event Action OnPressRotateLeft = delegate {  };
        public event Action OnPressRotateRight = delegate {  };
        public event Action OnPressOk = delegate {  };
        public event Action OnPressRestart = delegate {  };
        
        [SerializeField]
        private MenuSaveScoreInputBindings _bindings;

        private void Update() {
            if (Input.GetKeyDown(_bindings.RotateLeft)) {
                OnPressRotateLeft.Invoke();
            }
            
            if (Input.GetKeyDown(_bindings.RotateRight)) {
                OnPressRotateRight.Invoke();
            }

            if (Input.GetKeyDown(_bindings.Ok)) {
                OnPressOk.Invoke();
            }

            if (Input.GetKeyDown(_bindings.Restart)) {
                OnPressRestart.Invoke();
            }
        }
        
    }

}