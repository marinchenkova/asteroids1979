﻿using Asteroids.Core.Base;
using Asteroids.Core.Util;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.Data.UI {

    /// <summary>
    /// Set best score to label.
    /// </summary>
    public class BestScoreUi : BaseBehaviour {

        [SerializeField]
        private Text _bestScoreText;

        private void Awake() {
            var bestScore = TextUtils.FormatScore(ScoreSaver.GetMaxOfBestOrCurrentScore());
            _bestScoreText.text = bestScore;
        }

    }

}