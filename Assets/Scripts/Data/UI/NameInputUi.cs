﻿using System.Text;
using Asteroids.Core.Base;
using Asteroids.Data.Game;
using UnityEngine;
using UnityEngine.UI;

namespace Asteroids.Data.UI {

    /// <summary>
    /// Name input widget.
    /// </summary>
    public class NameInputUi : BaseBehaviour {

        [SerializeField] 
        private Text _label;

        private const string Pattern = "_ _ _";
        private const int AlphabetLength = 27;
        
        private readonly char[] _alphabet = {
            'a', 'b', 'c', 'd', 'e',
            'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o',
            'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 
            'z', '_'
        };
        
        private readonly StringBuilder _current = new StringBuilder();
        private int _placePointer = 0;
        private int _letterPointer = 0;
        private bool _done = false;

        private void Awake() {
            _current.Append(Pattern);
            Notify();
        }

        public void OnNextLetter() {
            if (_done) return;
            _letterPointer++;
            if (_letterPointer > AlphabetLength - 1) {
                _letterPointer = 0;
            }
            ReplaceCurrentPlace();
            Notify();
        }

        public void OnPrevLetter() {
            if (_done) return;
            _letterPointer--;
            if (_letterPointer < 0) {
                _letterPointer = AlphabetLength - 1;
            }
            ReplaceCurrentPlace();
            Notify();
        }

        public void OnLetterOk() {
            if (_done) return;
            
            _letterPointer = 0;
            _placePointer++;
            
            if (_placePointer > 2) {
                _done = true;
                ScoreSaver.SaveCurrentScoreTo(CurrentText());
            }
        }

        private void Notify() {
            _label.text = CurrentText();
        }

        private string CurrentText() {
            return _current.ToString();
        }

        private void ReplaceCurrentPlace() {
            var placeInString = PlaceInString();
            _current[placeInString] = GetCurrentLetter();
        }

        private int PlaceInString() {
            switch (_placePointer) {
                case 0: return 0;
                case 1: return 2;
                case 2: return 4;
            }

            return 0;
        }
        
        private char GetCurrentLetter() {
            return _alphabet[_letterPointer];
        }
        
    }

}