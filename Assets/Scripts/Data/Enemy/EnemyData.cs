﻿using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using UnityEngine;

namespace Asteroids.Data.Enemy {
    
    /// <summary>
    /// Describes enemy.
    /// </summary>
    [CreateAssetMenu(menuName = "Asteroids/EnemyData", fileName = "EnemyData")]
    public class EnemyData : BaseScriptable {

        
        [SerializeField]
        private float _minSpeed = 1f;
        public float MinSpeed => _minSpeed;

        [SerializeField]
        private float _maxSpeed = 1f;
        public float MaxSpeed => _maxSpeed;

        [SerializeField]
        private float _health = 1f;
        public float Health => _health;

        [SerializeField]
        private float _damage = 1f;
        public float Damage => _damage;

        [Tooltip("Score for destruction.")]
        [SerializeField]
        private int _score = 10;
        
        /// <summary>
        /// Score for destruction.
        /// </summary>
        public int Score => _score;

        [SerializeField]
        private VolumedClip _destroyClip;
        public VolumedClip DestroyClip => _destroyClip;
        
    }
    
}
