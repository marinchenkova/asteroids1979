﻿using Asteroids.Core.Audio;
using Asteroids.Core.Base;
using Asteroids.Core.Common;
using UnityEngine;

namespace Asteroids.Data.Enemy {

    /// <summary>
    /// Used to place destruction effect with <see cref="ParticleSystem"/> from pool when enemy is destroyed.
    /// </summary>
    public class EnemyDestructionEffectPlayer : BaseBehaviour {
        
        [SerializeField]
        private ObjectPool _destroyEffectPool;
        
        [SerializeField]
        private VolumedAudio _destroyEffectAudio;

        /// <summary>
        /// Perform destruction effect.
        /// </summary>
        public void PlayDestructionEffect(EnemyData data, Vector3 position) {
            var effect = _destroyEffectPool.GetElement();
            effect.transform.position = position;
            effect.GetComponent<ParticleSystem>().Play();
            _destroyEffectAudio.Play(data.DestroyClip);
        }

    }

}