﻿using Asteroids.Core.Common;
using UnityEngine;

namespace Asteroids.Data.Enemy {

    /// <summary>
    /// Health value is decreased by received damage when collides with <see cref="Damage"/>.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class Health : ObservableFloat {
        
        private void OnCollisionEnter2D(Collision2D other) {
            var damage = other.gameObject.GetComponent<Damage>();
            if (damage != null) ReceiveDamage(damage);
        }

        private void ReceiveDamage(Damage damage) {
            var prevHealthValue = Value;
            var nextHealthValue = Mathf.Clamp(prevHealthValue - damage.Value, 0f, 1f);
            Value = nextHealthValue;
            damage.OnDamageApplied(prevHealthValue - nextHealthValue);
        }

    }

}