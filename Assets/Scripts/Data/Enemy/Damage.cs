﻿using Asteroids.Core.Common;

namespace Asteroids.Data.Enemy {

    /// <summary>
    /// Causes <see cref="Health"/> decrease when collides with it.
    /// Health consumes damage and damage is decreased by consumed value.  
    /// </summary>
    public class Damage : ObservableFloat {
        
        /// <summary>
        /// Decrease damage by consumed value.
        /// </summary>
        public void OnDamageApplied(float value) {
            Value -= value;
        }
        
    }

}